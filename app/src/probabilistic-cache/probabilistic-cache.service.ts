import * as moment from 'moment';

import { Cache } from 'cache-manager';

import { Injectable, Inject, CACHE_MANAGER } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ProbabilisticCacheDto } from './probablistic-cache.dto';
import { ProbabilisticCacheEntity } from './probabilistic-cache.entity';

@Injectable()
export class ProbabilisticCacheService {
    private TTL: number = 30;

    constructor(
        @InjectRepository(ProbabilisticCacheEntity) private readonly repo: Repository<ProbabilisticCacheEntity>,
        @Inject(CACHE_MANAGER) private cacheManager: Cache
    ) {}

    getFromDb(id: number): Promise<ProbabilisticCacheEntity> {
        return new Promise(async (resolve, reject) => {
            setTimeout(() => {
                this.repo.findOne(id)
                    .then(resolve)
                    .catch(reject);
            }, 2000);
        })
    }

    async get(id: number) {
        const cache = await this.cacheManager.get<ProbabilisticCacheDto>(String(id));

        if (!cache || moment().unix() - cache.delta * Math.log(Math.random()) >= cache.expired) {
            const start = moment().unix();
            const expired = moment().add(this.TTL, 'seconds').unix();
            const value = await this.getFromDb(id);
            const delta = moment().unix() - start;

            this.cacheManager.set(
                String(value.id),
                new ProbabilisticCacheDto(value, delta, expired),
                { ttl: this.TTL }
            );

            return value;
        }

        return cache.value;
    }

    save(body) {
        const randomId = Math.random() * (1000000 - 1) + 1
        this.cacheManager.set(String(randomId), { value: randomId }, )
        return this.repo.save(new ProbabilisticCacheEntity({ value: randomId }))
    }
}
