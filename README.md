# Redis Cluster
## Redis, Redis Sentinel, Node.js, PostgreSQL, Artillery

## Run

- ```git clone https://gitlab.com/sytn1kk/redis-cluster.git```
- ```cd redis-cluster```
_ ```docker-compose -f "docker-compose.yml" up -d --build```
- ```docker run --network=redis-cluster_redis-cluster --rm -it \
      -v "$(pwd)/artillery/":/scripts \
      artilleryio/artillery:latest \
      run /scripts/load-test.yml```

## Screenshots

### Data replication
![Data replication](https://i2.paste.pics/26a7c135a3823da403ec781750dafae4.png "Data replication")

### Redis maxmemory-policy: allkeys-random and 20 mb memory size
![Redis maxmemory-policy](https://i2.paste.pics/1db33a9170fc725e3940d6f7d06cdd1b.png "Redis maxmemory-policy")
