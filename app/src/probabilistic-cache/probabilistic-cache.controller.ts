import { Controller, Post, Get, Param, Body } from '@nestjs/common';

import { ProbabilisticCacheService } from './probabilistic-cache.service';

@Controller('probabilistic-cache')
export class ProbabilisticCacheController {
    constructor(private probabilisticCacheService: ProbabilisticCacheService) {}

    @Get(':id')
    index(@Param('id') id) {
        return this.probabilisticCacheService.get(id);
    }
    
    @Post()
    save(@Body() body) {
        return this.probabilisticCacheService.save(body);
    }
}
